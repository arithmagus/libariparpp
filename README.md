# libariparpp

C++ Library for parsing and multiple evaluations of small arithmetical expressions

This library is meant for C++ programmed embedded devices, that need to do multiple evaluations of simple expressions in order to control a digital output.

Files in 'tests' are used to check the library after compilation.

