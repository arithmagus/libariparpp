PROJECT_ROOT = $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

OBJS = libAriPar.o parser.o opelem.o valelem.o expression.o qvalue.o

CPPFLAGS += -std=c++17 -fPIC

#ifeq ($(BUILD_MODE),debug)
  CFLAGS += -g
#else ifeq ($(BUILD_MODE),run)
#  CFLAGS += -O2
#else
#  $(error Build mode $(BUILD_MODE) not supported by this Makefile)
#endif

TARGETPATH=bin/debug/
TARGET=bin/debug/libAriPar

.PHONY: all tests

all:	$(TARGET) tests

tests: tests/simple_calculations tests/primitives
	@which cppcheck && cppcheck $(PROJECT_ROOT)/src/ 
	@LD_LIBRARY_PATH=$(PROJECT_ROOT)/bin/shared/:$(LD_LIBRARY_PATH) tests/primitives && LD_LIBRARY_PATH=$(PROJECT_ROOT)/bin/shared/:$(LD_LIBRARY_PATH) tests/simple_calculations

tests/primitives: tests/primitives.cpp bin/shared/libAriPar.so
	@mkdir -p $(dir $@)
	$(CXX) -L$(PROJECT_ROOT)/bin/shared/ -g -lAriPar -o $@ $<

tests/simple_calculations: tests/simple_calculations.cpp bin/shared/libAriPar.so
	@mkdir -p $(dir $@)
	$(CXX) -L$(PROJECT_ROOT)/bin/shared/ -g -lAriPar -o $@ $<

bin/shared/libAriPar.so: $(addprefix $(TARGETPATH)/, $(OBJS))
	@mkdir -p $(dir $@)
	$(CXX) -shared -o ${@} $^

$(TARGET):	$(TARGETPATH)/libAriPar.o $(addprefix $(TARGETPATH)/, $(OBJS))
	$(CXX) -o $@ $^

vpath %.cpp $(PROJECT_ROOT)/src
vpath %.c $(PROJECT_ROOT)/src
vpath %.hpp $(PROJECT_ROOT)/src
vpath %.h $(PROJECT_ROOT)/src

$(TARGETPATH)/%.o:	%.cpp %.hpp
	@mkdir -p $(dir $@)
	$(CXX) -c $(CFLAGS) $(CXXFLAGS) $(CPPFLAGS) -o $@ $<

$(TARGETPATH)/%.o:	%.c %.h
	@mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS) $(CPPFLAGS) -o $@ $<

clean:
	rm -fr $(addprefix $(TARGETPATH)/, libAriPar $(OBJS)) bin/shared/libAriPar.so
