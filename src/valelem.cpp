/*
 * ValElem.cpp
 *
 *  Created on: 29.08.2020
 *      Author: andre
 */

#include "valelem.hpp"

namespace aripar
{

  template<typename VType>
  std::string ValElem<VType>::s_name = "ValElem";

  template<>
  std::string ValElem<double>::s_name = "ValElem_Dbl";

  template<>
  std::string ValElem<QValue>::s_name = "ValElem_QVl";


} /* namespace aripar */
