/*
 * expression.cpp
 *
 *  Created on: 30.08.2020
 *      Author: Andre Dietrich
 */

#include "expression.hpp"
#include "valelem.hpp"
#include "parser.hpp"
#include <iostream>

namespace aripar
{

Expression::Expression(std::string text) :
		m_root(NULL)
{

#ifdef DEBUG
	std::cout << "text: " << text << std::endl;
#endif

	Parser p(m_root);

	p.parse(text);
}

double Expression::eval()
{
#ifdef DEBUG
	if (m_root)
		m_root->debug_walk(0);
#endif

	return (m_root) ? m_root->eval() : 0;
}

} /* namespace aripar */
