/*
 * ValElem.hpp
 *
 *  Created on: 29.08.2020
 *      Author: Andre Dietrich
 *
 */

#ifndef SRC_VALELEM_HPP_
#define SRC_VALELEM_HPP_

#include "token.hpp"
#include "qvalue.hpp"

namespace aripar
{

  template<typename VType>
  class ValElem: public Token
  {

    public:
      ValElem(VType value, size_t calc_group) :
          m_value(value)
      {
        m_calc_group = calc_group;
      };
      virtual ~ValElem() {};

      virtual double eval()
      {
        return m_value;
      }

      virtual std::string name() override
      {
        return ValElem<VType>::s_name;
      }

      virtual Type type() override
      {
        return Type::imidiate;
      }

      virtual uint16_t precedence()
      {
        return 0;
      }

      virtual void debug_walk(size_t level)
      {
        std::cout << std::string(level * 3, ' ') << std::string(3, '-')
            << name() << "[" << m_calc_group << "](" << m_value << ")"
            << std::endl;
      }

    private:

      static std::string s_name;
      QValue m_value;

  };

} /* namespace aripar */

#endif /* SRC_VALELEM_HPP_ */
