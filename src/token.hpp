/*
 * token.hpp
 *
 *  Created on: 30.08.2020
 *      Author: Andre Dietrich
 */

#ifndef SRC_TOKEN_HPP_
#define SRC_TOKEN_HPP_

#include <string>
#include <iostream>

namespace aripar {

class Token {
public:
	enum class Type : uint8_t { imidiate, operation, function };

	virtual double eval() = 0;
	virtual std::string name() = 0;
	virtual Type type() = 0;
	virtual size_t group() { return m_calc_group; }
	virtual uint16_t precedence() = 0;

	friend class Parser;

	virtual void debug_walk(size_t level)
	{
		//std::cout << "enter " << name() << std::endl;
		//std::cout << "left : ";
		if (m_left) m_left->debug_walk(level + 1);

		std::cout << std::string(level * 3, ' ') << std::string(3, '-') << name() << "[" << m_calc_group << "]" << std::endl;
		//std::cout << "right: ";
		if (m_right) m_right->debug_walk(level + 1);
		//std::cout << "calc " << name() << std::endl;
		//std::cout << "leave " << name() << std::endl;
	}

protected:
  size_t m_calc_group = 0;
	Token * m_top = NULL;
	Token * m_left = NULL;
	Token * m_right = NULL;

};

} /* namespace aripar */

#endif /* SRC_TOKEN_HPP_ */
