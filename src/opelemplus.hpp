/*
 * opelemplus.hpp
 *
 *  Created on: 30.08.2020
 *      Author: Andre Dietrich
 */

#ifndef SRC_OPELEMPLUS_HPP_
#define SRC_OPELEMPLUS_HPP_

#include "opelem.hpp"

namespace aripar {

class OpElemPlus : public OpElem {
public:
	OpElemPlus();
	virtual ~OpElemPlus();

	virtual double eval() override;
	virtual std::string name() override { return OpElemPlus::s_name; }
	virtual Type type() override { return Type::operation; }
	virtual Precedence precedence() override { return Precedence::addition; }

private:
	static std::string s_name;
};

} /* namespace aripar */

#endif /* SRC_OPELEMPLUS_HPP_ */
