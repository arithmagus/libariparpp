/*
 * parser.hpp
 *
 *  Created on: 29.08.2020
 *      Author: Andre Dietrich
 */

#ifndef SRC_PARSER_HPP_
#define SRC_PARSER_HPP_

#include <string>
#include "token.hpp"

namespace aripar {

class Parser {
public:
	Parser(Token *& root);
	virtual ~Parser();

	void parse(std::string& text);

private:

	template<typename CHAR>
	void step(CHAR c);

	void enlist_element( Token * newNode );
	void prepend_calc_group( Token * newNode );
	void append_calc_group( Token * newNode );
	void insert_before( Token * newNode, Token ** const branch );
	void append_left( Token * newNode, Token * const branch );
	void append_right( Token * newNode, Token * const branch );

	bool expecting_literal();

	size_t statemachine_status[2];

	size_t sm_step = 0;
	size_t sm_insert_element = 0;

	// temporary parser buffers
	int64_t 	  m_value;
	int8_t 		  m_decimals;
  bool        m_negative;
	std::string m_name;

	Token *&    m_root;
	Token *     m_current_token = NULL;

	ssize_t     m_current_calc_group = 1;

};

} /* namespace aripar */

#endif /* SRC_PARSER_HPP_ */
