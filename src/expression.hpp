/*
 * expression.hpp
 *
 *  Created on: 30.08.2020
 *      Author: Andre Dietrich
 */

#ifndef SRC_EXPRESSION_HPP_
#define SRC_EXPRESSION_HPP_

#include <string>

#include "token.hpp"

namespace aripar {

/**
 * \brief Expression class holds the complete arithmetic expression and its modifier functions
 *
 * This class holds the expression tokens as well as parsing, modify and executing functions to handle the
 * arithmetic expression.
 *
 */
class Expression {
public:
  /**
   * Expression constructor should be called with a complete and valid arithmetical expression
   *
   * \throws SyntaxException A Syntax Exception will be thrown if an error is found during parsing.
   *
   * @param text expression string
   */
	Expression(std::string text);
	virtual ~Expression() {};

	/**
	 * evaluation function of the arithmetical expression
	 *
	 * @return the calculated value of the complete expression tree
	 */
	double eval();

private:
	/** root anchor for expression tree */
	aripar::Token * m_root;

	/**
	 * parsing function to analyze and disassemble the expression string
	 *
	 * should only be called from constructor as a reparsing is not intended.
	 */
	void parse(std::string & text);

	void cleanup();

	std::string m_copy;

};

} /* namespace aripar */

#endif /* SRC_EXPRESSION_HPP_ */
