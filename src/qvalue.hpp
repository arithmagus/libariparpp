/*
 * qvalue.hpp
 *
 *  Created on: 03.01.2021
 *      Author: Andre Dietrich
 */

#ifndef SRC_QVALUE_HPP_
#define SRC_QVALUE_HPP_

#include <cstdint>
#include <iostream>
#include <limits>

namespace aripar
{

  /**
   * Implementation of a integer based floating point value
   *
   * This class holds a floating point value and provides suitable operands for
   * floating point calculation.
   *
   */
  class QValue
  {
    public:
      QValue();
      QValue(int value);
      QValue(const QValue & ) = default;
      QValue(int64_t value, int8_t decimal);
      QValue(double value, double epsilon = std::numeric_limits<double>::epsilon());

      virtual ~QValue();

      void change_decimal( int8_t decimal );

      //friend bool operator== (const QValue& q1, const QValue& q2);

      friend std::ostream& operator<<(std::ostream& os, const QValue& qval);
      friend QValue operator+(const QValue &q1, const QValue &q2);

      operator double() const {
        return ((m_decimal < 0)?((double)m_value / power_of_ten(-m_decimal)):((double)m_value * power_of_ten(m_decimal)));
      };
      //explicit operator double() const { return ((m_decimal < 0)?((double)m_value / power_of_ten(m_decimal)):((double)m_value * power_of_ten(m_decimal))); }

    private:
      int64_t m_value;
      int8_t m_decimal;

      /**
       * fast returning power of ten
       *
       * @param decimal place
       * @return power of ten
       */
      static uint64_t power_of_ten(uint8_t decimal)
      {
        static const uint64_t factors[] = {
            1,
            10,
            100,
            1000,
            10000,
            100000,
            1000000,
            10000000,
            100000000,
            1000000000,
            10000000000,
            100000000000,
            1000000000000,
            10000000000000,
            100000000000000,
            1000000000000000,
            10000000000000000,
            100000000000000000,
            1000000000000000000
        };

        if ( decimal < (sizeof(factors)/sizeof(factors[0])) )
          return factors[decimal];

        return 1;
      }

  };

} /* namespace aripar */

#endif /* SRC_QVALUE_HPP_ */
