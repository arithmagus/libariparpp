/**
 * parser.cpp
 *
 *  Created on: 29.08.2020
 *      Author: andre
 */

#include "parser.hpp"
#include "valelem.hpp"
#include "opelem.hpp"
#include <iostream>
#include "syntax_exception.hpp"

namespace aripar {

Parser::Parser(Token *& root) :
	m_root(root)
{}

Parser::~Parser() {
}

void Parser::parse(std::string& text)
{
	std::string::const_iterator it = text.begin();

	for ( char& c : text )
	{
		try {
			step(c);
		}
		catch (SyntaxException se)
		{
			throw(
					SyntaxException(
							Streamer()
							<< "syntax error @ char " << (&c - &text[0])
							<< " (" << text.substr(&c - &text[0], 20) << ")" >> Streamer::to_str )  );
		}

	}
	step('\0'); // end parsing

	if (m_current_calc_group > 1)
	{
    throw(
        SyntaxException(
            Streamer()
            << "syntax error: missing closing braket ')'"  ));
	}
	else if (m_current_calc_group < 1)
	{
    throw(
        SyntaxException(
            Streamer()
            << "syntax error: too many closing brakets ')'"  ));
	}
}

template<typename CHAR>
static inline bool parse_number( int64_t& value, int8_t& decimals, CHAR c ) {
	if (c == '.')
	{
		decimals = 0;
		return true;
	}
	else if (('0' <= c) && (c <= '9'))
	{
		if (decimals > -1) decimals++;
		value = value * 10 + (c - '0');
		return true;
	}

	return false;
}

template<typename CHAR>
static inline bool parse_name( std::string& name, CHAR c ) {

	if ((('a' <= c) && (c <= 'z')) ||
		(('A' <= c) && (c <= 'Z')) ||
		(('0' <= c) && (c <= '9')) ||
		 (c == '_'))
	{
		name += c;
		return true;
	}
	else
	{
		return false;
	}
}

void Parser::insert_before( Token * newNode, Token ** const branch )
{

	newNode->m_left = *branch;

	if (*branch)
	{
		newNode->m_top = (*branch)->m_top;
		(*branch)->m_top = newNode;
	}

	*branch = newNode;

}

void Parser::append_left( Token * newNode, Token * const branch )
{

	if (!branch) return;

	newNode->m_left = branch->m_left;

	if (branch->m_left)
	{
		branch->m_left->m_top = newNode;
	}

	newNode->m_top = branch;
	branch->m_left = newNode;

}

void Parser::append_right( Token * newNode, Token * const branch )
{

	if (!branch) return;

	//add old node to new nodes left anchor, as next element will be added right
	newNode->m_left = branch->m_right;

	if (branch->m_right)
	{
		branch->m_right->m_top = newNode;
	}

	newNode->m_top = branch;
	branch->m_right = newNode;

	m_current_token = newNode;

}

void Parser::prepend_calc_group( Token * newNode )
{

	/**
	 * append new node in front, when tree is empty.
	 */
	if ( !m_root )
	{
		m_root = newNode;
		return;
	}

	if ( !m_current_token )
	{
		m_current_token = m_root;
	}

	Token * walker = m_current_token;



	while ((walker) && (walker->group() > newNode->group()))
	{
		walker = walker->m_top;
	}

	while ((walker) && (walker->group() >= newNode->group()) && (walker->precedence() <= newNode->precedence()))
	{
		walker = walker->m_top;
	}

	if (walker)
	{
		append_right( newNode, walker );
	}
	else
	{
		insert_before( newNode, &m_root );
	}

	m_current_token = newNode;

}

void Parser::append_calc_group( Token * newNode )
{

	/**
	 * append new node in front, when tree is empty.
	 */
	if ( !m_root )
	{
		m_root = newNode;
		return;
	}

	if ( !m_current_token )
	{
		m_current_token = m_root;
	}

	Token * walker = m_current_token;

	while ((walker) && (walker->group() <= newNode->group()))
	{
		walker = walker->m_top;
	}

	if (walker->m_top)
	{
		insert_before( newNode, &walker->m_top );
	}
	else
	{
		insert_before( newNode, &m_root );
	}


}


/**
 * function specific state machine helper, should work on most modern C++ compilers
 */
#define STATEMACHINE_START(id) \
	static_assert( id < sizeof(statemachine_status)/sizeof(statemachine_status[0]), "insufficient size of sm_status!"); \
	static uint8_t _STATEMACHINE_ID = id; \
	switch(statemachine_status[_STATEMACHINE_ID]) { default:
#define yield(x) { statemachine_status[_STATEMACHINE_ID] = __LINE__; return x; case __LINE__:; }
#define STATEMACHINE_END() }

void Parser::enlist_element(Token * newNode)
{

#ifdef DEBUG
	if (m_root) m_root->debug_walk(0);
#endif

#ifdef DEBUG
	if (newNode) std::cout << "found: " << newNode->name() << std::endl;
#endif

	STATEMACHINE_START(1);

	if (m_root == NULL)
	{
		if (newNode->type() == Token::Type::operation)
		{
			throw(SyntaxException("First element cannot be operation in infix notation."));
		}
		m_root = newNode;
		yield();
	}

	while (newNode)
	{
		if (newNode->type() != Token::Type::operation)
		{
			throw(SyntaxException("operator expected."));
		}
		else
		{
			prepend_calc_group( newNode );
		}

		yield();

		if (newNode->type() == Token::Type::operation)
		{
			throw(SyntaxException("value giving token expected."));
		}
		else
		{
			append_right( newNode, m_current_token );
		}

		yield();
	}

	STATEMACHINE_END();


}

template<typename CHAR>
inline bool is_whitespace(CHAR c)
{
  switch(c) {
    case ' ':
    case '\t':
    case '\r':
    case '\n':
      return true;
    default:
      return false;
  }
}

inline bool Parser::expecting_literal()
{
  return ((m_root == NULL) || ((m_current_token) && (m_current_token->type() == Token::Type::operation)));
}

template<typename CHAR>
void Parser::step(CHAR c) {
	uint8_t pass = 0;
	uint8_t maxpasses = 1;
	bool negative_literal = false;

	STATEMACHINE_START(0);

	do {
		if ((('0' <= c) && (c <= '9')) || ((c == '-') && (negative_literal == true)))
		{
		  m_negative = negative_literal;
			m_value = 0;
			m_decimals = -1; // within this parser, -1 means no decimals - to commit this value to qvalue it has to be negated.
			if (c == '-') yield(); // if negative flag is set, continue with next char
			while (parse_number( m_value, m_decimals, c ))
			{
				yield();
			}
			if (m_negative) m_value *= -1;
			ValElem<QValue> * newValue = new ValElem<QValue>(QValue(m_value, (m_decimals<0)?0:-m_decimals), m_current_calc_group);
			enlist_element(newValue);
		}
		else if ((('a' <= c) && (c <= 'z')) ||
				 (('A' <= c) && (c <= 'Z')) ||
				  (c == '_'))
		{
			m_name.clear();
			while (parse_name( m_name, c ))
			{
				yield();
			}
		}
		else if (is_whitespace(c))
		{
			// just whitespace, don't do anything
			yield();
		}
		else if (c == '+')
		{
			{
				Token * newValue = new OpElem<Operator::add>(m_current_calc_group);
				enlist_element(newValue);
			}
			yield();
		}
		else if (c == '*')
		{
			{
				Token * newValue = new OpElem<Operator::mul>(m_current_calc_group);
				enlist_element(newValue);
			}
			yield();
		}
		else if (c == '-')
		{
		  if (expecting_literal())
		  {
		    negative_literal = true;
		    maxpasses = 2; // allow second pass
		    continue;
		  }
		  else
			{
				Token * newValue = new OpElem<Operator::sub>(m_current_calc_group);
				enlist_element(newValue);
			}
			yield();
		}
		else if (c == '/')
		{
			{
				Token * newValue = new OpElem<Operator::div>(m_current_calc_group);
				enlist_element(newValue);
			}
			yield();
		}
		else if (c == '(')
		{
		  ++m_current_calc_group;
			yield();
		}
		else if (c == ')')
		{
		  --m_current_calc_group;
			yield();
		}
	} while (pass++ < maxpasses);
	// leave parser, if the character could not be processed even in the second pass

	STATEMACHINE_END();

	if (c != '\0')
		throw(SyntaxException("Invalid character."));
}



}
