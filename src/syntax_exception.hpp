/*
 * syntax_exception.hpp
 *
 *  Created on: 15.09.2020
 *      Author: Andre Dietrich
 */

#ifndef SRC_SYNTAX_EXCEPTION_HPP_
#define SRC_SYNTAX_EXCEPTION_HPP_

#include <string>
#include <stdexcept>
#include "streamer.hpp"

namespace aripar {

class SyntaxException : public std::runtime_error
{
public:
	SyntaxException(std::string message) :
		std::runtime_error(message)
	{};

};

} /* namespace aripar */

#endif /* SRC_SYNTAX_EXCEPTION_HPP_ */
