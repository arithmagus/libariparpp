/*
 * qvalue.cpp
 *
 *  Created on: 03.01.2021
 *      Author: Andre Dietrich
 */

#include "qvalue.hpp"

namespace aripar
{

  QValue::QValue()
  {
  }

  QValue::QValue(int value) :
      m_value(value), m_decimal(0)
  {
  }

  QValue::QValue(int64_t value, int8_t decimal) :
      m_value(value), m_decimal(decimal)
  {
  }

  QValue::QValue(double value, double epsilon) :
      m_value(value), m_decimal(0)
  {
    //static const double epsilon = 0.001;

    int_fast8_t sign = 1;
    if (value < 0)
      sign = -1;
    value *= sign;

    int64_t target = value;
    uint64_t divider = 1;

    double tmp = (value - target);
    //double reps = epsi

    double test = target;

    while ((test < (value - epsilon)) || (test > (value + epsilon)))
    {
      tmp *= 10;
      uint8_t digit = tmp;
      tmp -= digit;

      // check if already in range of epsilon
      if (digit > 5)
      {
        if (((test + 1) > value) && ((test + 1) < value + epsilon))
        {
          target += 1;
          break;
        }
      }

      target = target * 10 + digit;
      divider *= 10;
      m_decimal--;
      test = (double) target / divider;
    }

    m_value = target * sign;
  }

  QValue::~QValue()
  {
  }

  /**
   * change the decimal place to the mentioned
   *
   * @param decimal
   */
  void QValue::change_decimal( int8_t decimal )
  {
    if (m_decimal < decimal)
    {
      m_value /= power_of_ten(decimal - m_decimal);
      m_decimal = decimal;
    }
    else if (m_decimal > decimal)
    {
      m_value *= power_of_ten(m_decimal - decimal);
      m_decimal = decimal;
    }
  }

  std::ostream& operator<<(std::ostream &os, const QValue &qval)
  {
    os << qval.m_value << '/' << static_cast<unsigned>(qval.m_decimal);
    return os;
  }

  /**
   * comparison operator
   *
   * @param q1 first QValue
   * @param q2 second QValue
   * @return returns true if both have the same value
   */
  /*bool operator==(const QValue &q1, const QValue &q2)
  {
    if (q1.m_decimal == q2.m_decimal)
      return (q1.m_value == q2.m_value);

      QValue qtmp1(q2);
      qtmp1.change_decimal(q1.m_decimal);

      QValue qtmp2(q1);
      qtmp2.change_decimal(q2.m_decimal);

      return ((qtmp1.m_value == q1.m_value) && (qtmp2.m_value == q2.m_value));
  }*/

  /**
   * addition operator
   *
   * @param q1 first QValue
   * @param q2 second QValue
   * @return returns sum of both values
   *
   * @todo: optimize precision
   */
  QValue operator+(const QValue &q1, const QValue &q2)
  {
    QValue qval;

    if (q1.m_decimal == q2.m_decimal)
    {
      qval.m_value = q1.m_value + q2.m_value;
    }
    else if (q1.m_decimal < q2.m_decimal)
    {
      qval = q2;
      qval.change_decimal(q1.m_decimal);
      qval.m_value += q1.m_value;
    }
    else
    {
      qval = q1;
      qval.change_decimal(q2.m_decimal);
      qval.m_value += q2.m_value;
    }

    return qval;
  }



} /* namespace aripar */
