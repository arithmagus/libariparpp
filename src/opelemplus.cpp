/*
 * opelemplus.cpp
 *
 *  Created on: 30.08.2020
 *      Author: Andre Dietrich
 */

#include "opelemplus.hpp"

namespace aripar {

OpElemPlus::OpElemPlus() {
}

OpElemPlus::~OpElemPlus() {
}

double OpElemPlus::eval() {
	return
			((m_left)?m_left->eval():0) +
			((m_right)?m_right->eval():0);
}

std::string OpElemPlus::s_name = "OpElemPlus";

} /* namespace aripar */
