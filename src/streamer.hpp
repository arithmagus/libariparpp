/*
 * streamer.hpp
 *
 *  Created on: 23.08.2020
 *      Author: Andre Dietrich
 */

#ifndef SRC_STREAMER_HPP_
#define SRC_STREAMER_HPP_

#include <sstream>

namespace aripar {

class Streamer {
public:
	Streamer() {}
	virtual ~Streamer() {}

    template <typename Type>
    Streamer & operator << (const Type & value)
    {
        m_stream << value;
        return *this;
    }

    std::string str() const         { return m_stream.str(); }
    operator std::string () const   { return m_stream.str(); }

    enum ConvertToString
    {
        to_str
    };
    std::string operator >> (ConvertToString) { return m_stream.str(); }

private:
    std::stringstream m_stream;

    Streamer(const Streamer &);
    Streamer & operator = (Streamer &);

};

} /* namespace aripar */

#endif /* SRC_STREAMER_HPP_ */
