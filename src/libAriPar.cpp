

#include "expression.hpp"
#include "libAriPar.hpp"
#include <iostream>

int main(int argc, char **argv) {

	if (argc > 1)
	{
		//argv[1];
	}
	else
	{
		std::cerr << "usage: " << argv[0] << " <arithmetical expression>" << std::endl;
		return -1;
	}

	//aripar::Expression exp(argv[1]);
	//aripar::Expression exp("2* 4   + 3 + 2 * 5 + 2");
  aripar::Expression exp("2* (4 + 3) * ( 7+3   *2) - 3 * (2 + ( 5 + (2*2))) + 5 + 3 * (8 + (2 * (3+3)))");

  aripar::Expression("3+3").eval();

	std::cout << "result: " << exp.eval() << std::endl;

	return 0;
}
