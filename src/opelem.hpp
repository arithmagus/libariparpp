/*
 * OpElem.hpp
 *
 *  Created on: 29.08.2020
 *      Author: andre
 */

#ifndef SRC_OPELEM_HPP_
#define SRC_OPELEM_HPP_

#include "token.hpp"
#include "syntax_exception.hpp"

namespace aripar
{

namespace Operator
{

struct add
{
  static constexpr char name[] = "addition";
  static const uint8_t preference = 2;
  static double eval(double x, double y)
  {
    return (x + y);
  }

};

struct sub
{
  static constexpr char name[] = "substraction";
  static const uint8_t preference = 2;
  static double eval(double x, double y)
  {
    return (x - y);
  }

};

struct mul
{
  static constexpr char name[] = "multiplication";
  static const uint8_t preference = 1;
  static double eval(double x, double y)
  {
    return (x * y);
  }

};

struct div
{
  static constexpr char name[] = "division";
  static const uint8_t preference = 1;
  static double eval(double x, double y)
  {
    return (x / y);
  }

};

}

template<typename Operation>
class OpElem: public Token
{
public:

  OpElem(size_t calc_group)
  {
    m_calc_group = calc_group;
  }

  virtual ~OpElem()
  {
  }

  double eval()
  {
    if ((!m_left) || (!m_right))
      throw SyntaxException("missing value");

    return Operation::eval(m_left->eval(), m_right->eval());
  }

  virtual std::string name() override
  {
    return Operation::name;
  }

  virtual Type type() override
  {
    return Type::operation;
  }

  virtual uint16_t precedence()
  {
    return Operation::preference;
  }

private:

};

} /* namespace aripar */

#endif /* SRC_OPELEM_HPP_ */
