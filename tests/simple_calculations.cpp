/**
 *
 * simple_calculations.cpp
 *
 * testing calculation functionality of libAriParpp
 *
 */

#include "../src/libAriPar.hpp"

int testcase(std::string name, std::string text, double compare_value, bool correct_expression )
{
  // prevent reordering
  asm volatile("" : : : "memory");

  try
  {
    double result = 0.0;
    if ((result = aripar::Expression(text).eval()) == compare_value)
    {
      std::cout << "Test '" << name << "' \033[1;32m[ successful ]\033[0m" << std::endl;
    }
    else
    {
      std::cerr << "Test '" << name << "' \033[1;31m[   failed   ]\033[0m" << std::endl;
      std::cerr << "expression '" << text << "' - result '" << result << "' - compare '" << compare_value << "'" << std::endl;
      return 1;
    }
  }
  catch(const std::runtime_error& e)
  {
    if (correct_expression)
    {
      std::cerr << "Test '" << name << "' \033[1;31m[   failed   ]\033[0m" << std::endl;
      std::cerr << "expression '" << text << "' has thrown an unexpected exception '" << e.what() << "'" << std::endl;
      return 1;
    }
    else
    {
      std::cout << "Test '" << name << "' \033[1;32m[ successful ]\033[0m exception detected." << std::endl;
    }
  }

  return (correct_expression)?0:1;
}

int main(int argc, char* argv[])
{

  int results = 0;

  std::cout << "Testing Arithmetics ..." << std::endl;

  results += testcase("B01 simple addition", "3+3", 6, true);
  results += testcase("B02 simple substraction", "4-3", 1, true);
  results += testcase("B03 simple multiplication", "3*3", 9, true);
  results += testcase("B04 float division", "10/3", 10.0/3, true);
  results += testcase("B05 float multiplication", "10.0*3.5", 35, true);
  results += testcase("B06 wildcard handling", " 13.21 + \t\r\n1.0", 14.21, true);
  results += testcase("B07 long expression", "2* (4 + 3) * ( 7+3   *2) - 3 * (2 + ( 5 + (2*2))) + 5 + 3 * (8 + (2 * (3+3)))", 214, true);
  results += testcase("B08 Syntax failure", " +4", 0, false);
  results += testcase("B09 unfinished expression", " 4+4*", 0, false);
  results += testcase("B10 simple negative value", " -3", -3.0, true);
  results += testcase("B11 addition of negative literal", "40+-37", 3, true);
  results += testcase("B12 unknown token", " 3 * MISTAKE + 2", -3.0, false);

  return 0;
}
