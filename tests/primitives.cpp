/*
 * primitives.cpp
 *
 *  Created on: 04.01.2021
 *      Author: Andre Dietrich
 */

#include "../src/qvalue.hpp"
#include <iostream>

template<typename T1, typename T2>
int testcase(std::string name, T1 q1, T2 q2, bool expectation )
{
  // prevent reordering
  asm volatile("" : : : "memory");

  bool result = false;
  if (result = ((q1 == q2) == expectation))
  {
    std::cout << "Test '" << name << "' \033[1;32m[ successful ]\033[0m" << std::endl;
  }
  else
  {
    std::cerr << "Test '" << name << "' \033[1;31m[   failed   ]\033[0m" << std::endl;
    std::cerr << "values '" << q1 << "' and '" << q2 << "' do " << ((expectation)?"not ":" ") << "match, which was not expected." << std::endl;
    return 1;
  }
  return 0;
}

int main(int argc, char* argv[])
{

  int results = 0;

  std::cout << "Testing QValue ..." << std::endl;

  results += testcase("A01 simple integer", aripar::QValue((int64_t)3,0), aripar::QValue((int64_t)3,0), true );
  results += testcase("A02 floating decimal", aripar::QValue((int64_t)50,0), aripar::QValue((int64_t)5000,-2), true );
  results += testcase("A03 simple double", aripar::QValue(123.456), aripar::QValue((int64_t)123456,-3), true );
  results += testcase("A04 simple fraction", aripar::QValue(1.0/8), aripar::QValue((int64_t)125,-3), true );
  results += testcase("A05 simple periodical", aripar::QValue(1.0/3,0.001), aripar::QValue((int64_t)333,-3), true );
  results += testcase("A06 negative float", aripar::QValue(10.0/3,0.001), aripar::QValue((int64_t)3333,-3), true );
  results += testcase("A07 double precision", aripar::QValue(3.141592653), aripar::QValue((int64_t)3141592653,-9), true );
  results += testcase("A08 irrational mismatch", aripar::QValue(243.0/13), 243.0/13, true );
  results += testcase("A09 epsilon on irrational", aripar::QValue(243.0/13,0.001), aripar::QValue((int64_t)18692,-3), true );
  results += testcase("A10 qvalue addition", aripar::QValue((int64_t)123, 0) + aripar::QValue((int64_t)456, -3), aripar::QValue((int64_t)123456,-3), true );
  results += testcase("A11 qvalue double conversion", aripar::QValue((int64_t)123, -3), 0.123, true );

  return (results == 0)?0:-1;
}



